import logging
import pandas as pd
import sys

# developed python functions
from dlv_helpers import *
from graph_helpers import *
from pilot import *

def compute_trust_updates(agents_trust, share_pred,params,G,feedback,penalty_factor=0.7,trust=1):
    """The core trust model to update agents' trust values
    Parameters
    ----------
    agents_trust: dict
        The dictionary item that stores agents and their corresponding trust values
    share_pred: str
        The share predicate used in the DLV program
    params: list
        A list of path files for dl files to include
    G: networkx.Graph
        The graph representing a particular scenario that includes models and the rules being applied
        to achieve these models
    feedback: str
        allow or deny decision that reflects the user feedback
    penalty_factor: float between 0 and 1, optional
        The penalty factor used in the decrease step. The higher the value is, the higher the punishment will be.
    trust: 0 or 1, optional
        When the trust value is 1, the update trust phase is enabled    
    
    Returns
    -------
    (dec,new_agents_trust): (str,dict)
        The sharing decision of the agent (allow or deny) and the final trust values of the agents
    """

    # initialise with the current trust values
    new_agents_trust = agents_trust.copy()
    
    params.insert(2,'-filter='+share_pred+',not_'+share_pred+',best_values')
    output = subprocess.check_output(params)
    
    if trust:
        logging.info("running with trust model")
    else:
        logging.info("running without trust model")
    
    logging.info("current trust values %s", new_agents_trust)
    logging.info("model: %s",output.replace("\n",""))
    
    output = output.split('{')[1].split('}')[0]
    results = output.split(', ')
    
    share_models = []
    nshare_models = []
    c_best_share = 0
    c_best_nshare = 0
    
    for model in results:
        cval = int(model[:-1].split(",")[-1])
        if model.startswith("share"):
            share_models.append((model,cval))
        elif model.startswith("not_share"):
            nshare_models.append((model,cval))
        elif model.startswith("best_values"):
            c_best_share,c_best_nshare = model[:-1].split("(")[1].split(",")
            c_best_share = int(c_best_share)
            c_best_nshare = int(c_best_nshare)
    
    dec="allow"
    logging.info("best values: %s %s",c_best_share,c_best_nshare)
    
    # share model does not exist
    if c_best_share == 0 and c_best_nshare == 0: 
        dec="deny"
        logging.info("no best share models exist, no update, return old values")
        # no trust updates needed
        #return dec,agents_trust
    
    # deny overrides
    if c_best_share <= c_best_nshare:
        dec = "deny"
    
    logging.info("model decision: %s truth:%s",dec,feedback)
    # update needed
    if dec == "allow" and feedback == "deny" and trust == 1:
        # compute average confidence value
        teta = (c_best_share + c_best_nshare)*1.0 / 2
        
        logging.info("trust update starts")
        logging.info("best_share: %s, best_nshare: %s, teta: %s",c_best_share, c_best_nshare, teta)
        
        import math
        ### decreasing trust
        logging.info("decreasing step starts") 
        ### if there are N share models above threshold, take the minimum trust value
        dec_trusts = {}
        for sm in share_models:
            logging.info(">>> share model %s",sm)
            if sm[1] > teta:
                # get relevant agents-cvs
                acvs = relevant_agents(sm[0],G)
                logging.info(">>> relevant agents in the model with confidence value %s",acvs)
                
                for acv in acvs:
                    # eq1: new trust value for the relevant agent
                    t = int(round(penalty_factor * math.floor((teta * 100)/acv[1])))
                    if t < 0:
                        t = 0
                    
                    logging.info(">>> new trust value %s",t)
                    # if a trust value exists for the agent already, keep the minimum value
                    if acv[0] in dec_trusts.keys():
                        val = dec_trusts[acv[0]]
                        if t<val:
                            dec_trusts[acv[0]] = t
                    else:
                        dec_trusts[acv[0]] = t
                     
        logging.info("after decreasing step: %s",dec_trusts)            
        
        ### increasing trust
        logging.info("increasing step starts") 
        ### if there are N best notshare models, take the minimum trust value
        inc_trusts = {}
        for nsm in nshare_models:
            logging.info(">>> not share model %s",nsm)
            # get the best -share model(s)
            if nsm[1] == c_best_nshare:
                # get relevant agents-cvs
                acvs = relevant_agents(nsm[0],G)
                logging.info(">>> relevant agents in the model with confidence value %s",acvs)
                
                for acv in acvs:
                    # eq2: new trust value for the relevant agent
                    t = int(round(math.ceil((teta * 100)/acv[1])))
                    
                    if t>100:
                        t=100
                    
                    logging.info(">>> new trust value %s",t)
                    # if a trust value exists for the agent already in inc_trusts, keep the minimum value
                    if acv[0] in inc_trusts.keys():
                        val = inc_trusts[acv[0]]
                        if t<val:
                            inc_trusts[acv[0]] = t
                    else:
                        inc_trusts[acv[0]] = t
                                        
        logging.info("after increasing step: %s",inc_trusts)             
        
        # compute final trust values for the agents
        # increase trust values overwrite decrease trust values
        # if agents are not involved in any models, keep the current trust values
                
        for a in agents_trust.keys():
            if a in inc_trusts.keys():
                new_agents_trust[a] = inc_trusts[a]
                logging.info(">>> negative feedback for %s inc taken",a)
            elif a in dec_trusts.keys():
                new_agents_trust[a] = dec_trusts[a]
                logging.info(">>> negative feedback for: %s dec taken",a)
                
        logging.info("after trust update: %s", new_agents_trust)
    
    return dec,new_agents_trust
    
def run_phone_scenario():
    """Running the phone scenario as described in the paper""" 
       
    # phone scenario
    agents_trust = {"boss":75, "sensor":90, "punch":65, "cctv":85}
    
    feedback = "deny"
    
    dl_paths = ['phone/trust-rules.dl']
    path = './phone/'

    node_preds,rule_preds,edges,params = run_dlv(agents_trust,dl_paths,path)

    G=generate_graph(node_preds,rule_preds,edges)[0]
    export_expl_graph('phone_share_scenario',G, path=path) #generates all explanation graph
    share_pred = 'share_details'

    new_agents_trust = compute_trust_updates(agents_trust,share_pred,params,G,feedback)[1]
    print new_agents_trust

def run_scenario(agents_trust,dl_paths,path,feedback,trust=1):    
    """Running a scenario from the pilot study
    Parameters
    ----------
    agents_trust: dict
        The dictionary item that stores agents and their corresponding trust values
    dl_paths: set
        The set of paths of dl files to be used in execution
    path: str
        The location of the main file to execute
    feedback: allow or deny
        The user feedback
    trust: 0 or 1, optional
        When it is 1, trust update is enabled
    
    Returns
    ----------
    (dec,new_agents_trust): (str,dict)
        The sharing decision of the agent (allow or deny) and the final trust values of the agents    
    """ 
    
    node_preds,rule_preds,edges,params = run_dlv(agents_trust,dl_paths,path)

    G=generate_graph(node_preds,rule_preds,edges)[0]
    #export_expl_graph('survey_share_scenario',G) #generates all explanation graph
    share_pred = 'share'

    dec,new_agents_trust = compute_trust_updates(agents_trust,share_pred,params,G,feedback,trust=trust)
    return dec,new_agents_trust
        
def run_survey():
    """Running 14 scenarios for 25 participants
    The trust update values will be saved as a plot.
    """
    df_scenarios = pd.read_csv('./pilot_study/data/survey-ex')
        
    agents_trust = {"c":0, "ps":0, "is":0, "sw":0, "sp":0, "fr":0, "ts":0, "fs":0}
    scenarios,agents_cvs = scenario_generator_all(agents_trust.keys(),df_scenarios)
    df_scenXlabels = get_true_labels(df_scenarios)

    # the number of how many people to include from 1 to part_no
    part_no = df_scenarios.shape[0]/14
    
    participants_info = dict()

    logging.info("LOGGING INITIAL PARAMETERS:")
    logging.info("AGENT PROFILES: %s", agents_cvs)

    init_trust_values = [40,50,60,70,80,90,100]

    for tr in init_trust_values: 
        print "trust: ", str(tr)   
        logging.info("TRUST INFO: %s \n", tr)
            
        for part in range(1,part_no+1):
            #print "part no: ",str(part)
            agents_trust = {"c":tr, "ps":tr, "is":tr, "sw":tr, "sp":tr, "fr":tr, "ts":tr, "fs":tr}
            new_trusts = agents_trust.copy()
            part_labels = df_scenXlabels.loc[:,part]
            
            # initilise a dict object for the specific trust value
            if not part in participants_info.keys(): 
                participants_info[part] = {tr:{}}
                
            all_new_trusts = []
            all_decisions = []
                        
            for i,key in enumerate(scenarios.keys()):
                path = './pilot_study/'
                
                # get the scenario
                update_scenario_file(scenarios[key],path)
                logging.info("\n PARTICIPANT NO: %s SCENARIO: %s", part,key)
                
                dl_paths = ['pilot_study/survey-rules.dl', 'pilot_study/scenario.dl']
               
                # keep all trust updates
                all_new_trusts.append(new_trusts.values())
                
                dec,new_trusts = run_scenario(new_trusts,dl_paths,path,part_labels[i+1])
                
                # keep all decisions
                all_decisions.append([dec,part_labels[i+1]])
                                
            participants_info[part][tr] = (all_new_trusts,all_decisions)
            
    for tr in init_trust_values:
        log_plots(agents_trust.keys(),participants_info, tr)

################################ MAIN PROGRAM
# LOGGING execution details
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.basicConfig(filename="logs/run.log", filemode='w', level=logging.INFO)
################################

# get the scenario input from the command line
sname = sys.argv[1]

if sname == "phone":
    run_phone_scenario()
elif sname == "pilot":
    run_survey()


