import subprocess
import copy

def find_predicates(text):
    """The predicates are parsed from a text
    Parameters
    ----------
    text: str
        The DLV output including all predicates
    
    Returns
    ----------
    pred_set: list
        A list of predicates
    """
    
    lpc = 0 # left paranthesis count
    rpc = 0 # left paranthesis count
    brindex = 0
    temp = ""
    
    pred_set = list()
        
    for ind,ch in enumerate(text):
        temp = temp + ch
        if ch == "," and lpc==0:
            pred_set.append(temp[:-1])
            temp = ""     
        if ch == "(":
            lpc = lpc +1
        if ch == ")":
            rpc = rpc +1              
        if lpc == rpc and lpc>0:
            lpc = 0
            rpc = 0
        if ind+1 == len(text):
            pred_set.append(temp)
    
    return pred_set
    
def read_rules(filename,path):
    """The rules are used to export explanation graphs
    Parameters
    ----------
    filename: str
        The filename that includes the explanation rules
    path: str
        The location of the rules file
    
    Returns
    ----------
    rule_dict: dict
        A dictionary of rulenames and the representation of rules
    """
    
    rule_dict = dict()
    
    f = open(path+filename, "r")
    
    # first comp: rule scheme
    # second: vars used in the scheme
    # third comp: head
    # rest: body
    
    for rule in f:
        rule = rule.replace('\n', '')
        r = rule.split(';')
        rname = r[0].split(",")[0]
        variables = r[0].split("[")[1].split("]")[0].split(",") 
        rule_dict[rname] = tuple([r[0],variables,r[1],r[2:]])
      
    f.close()
    
    return rule_dict

def parse_dlv(rset,path):
    """The DLV output is parsed to have structured outputs
    Parameters
    ----------
    rset: str
        The resultset obtained from the DLV tool
    path: str
        The location of execution path
    
    Returns
    ----------
    (node_preds,rule_preds,edges): (set,set,set)
        The set of nodes, the set of rules and the set of edges that will 
        be used in the graph representation
    """
    
    node_preds = set()
    rule_preds = set()
    att_preds = set()
    edges = set()
    
    rset = rset.split('{')[1].split('}')[0]
    predset = rset.split(', ')
    
    for pred in predset:
        if pred.startswith("rule("):
            rule_preds.add(pred)
            att_preds.add(pred)
        else:
            if not (pred.startswith("minimise(") or pred.startswith("best_values")):
                node_preds.add(pred)
    
    # read rules from file
    rule_dict = read_rules('rules.txt',path)
    
    for rule_pred in rule_preds:
        #rname = rule_pred[:-1] # as it is kept in the dict object
        #if "," in rule_pred:
        rname = rule_pred.split(",")[0]
        
        # scheme components
        rscheme = rule_dict[rname][0]
        rvars = rule_dict[rname][1] # get scheme variables
        rhead = rule_dict[rname][2]
        rbody = rule_dict[rname][3] # set
        
        pred_text = rule_pred.split("[")[1].split("]")[0]
        
        rp = dict()
        for i,v in enumerate(rvars):
            rp[v] = find_predicates(pred_text)[i]
        
        rheadnew = copy.copy(rhead)
        rbodynew = copy.copy(rbody)
        for v in rvars:
            rheadnew = rheadnew.replace(v,rp[v])
            for i,b in enumerate(rbodynew):
                rbodynew[i] = b.replace(v,rp[v])
                
        # populating edges
        edges.add((rule_pred,rheadnew))
        for b in rbodynew:
            edges.add((b,rule_pred))
        
    return node_preds,rule_preds,edges

def update_trust_file(agents_trust,path):
    """The trust file is generated and saved to the specified path
    Parameters
    ----------
    agents_trust: dict
        The dictionary item that stores agents and their corresponding trust values
    path: str
        The location of the trust file to be saved
    """
    
    trust = ""
    for a in agents_trust.keys():
        trust += "#const trust_"+a+"="+str(agents_trust[a])+".\n"
        trust += "trust("+a+",trust_"+a+").\n"
    
    f= open(path+'/trust.dl','w+')
    f.write(trust)
    f.close()
    
def run_dlv(agents_trust,dl_files_set,path):
    """Running the DLV tool
    Parameters
    ----------
    agents_trust: dict
        The dictionary item that stores agents and their corresponding trust values
    dl_files_set: set
        The set of paths of dl files to be used in execution
    path: str
        The location of the main file to execute
    Returns
    ----------
    (node_preds,rule_preds,edges,params): (set,set,set,set)
        The set of nodes, the set of rules and the set of edges that will 
        be used in the graph representation and the set of parameters used to run the tool
    """
    
    params = list()
    
    # generate trust file
    update_trust_file(agents_trust,path)   
    
    # append trust values
    params.append(path+'/trust.dl')
    
    for dl in dl_files_set:
        params.append(dl)
    
    params.insert(0,'./dlv') # requires dlv to run from terminal
    params.insert(1,'-silent')
    
    output = subprocess.check_output(params)
        
    node_preds,rule_preds,edges = parse_dlv(output,path)
    return node_preds,rule_preds,edges,params
