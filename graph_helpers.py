import networkx as nx
from networkx.algorithms.traversal.depth_first_search import dfs_tree
import pygraphviz as pgv

def generate_graph(node_preds,rule_preds,edges,export_json=0):
    """A graph is generated according to the nodes and the edges provided
    Parameters
    ----------
    node_preds: set
        The node predicates of the graph
    rule_preds: set
        The rule node predicates of the graph
    edges: set
        The edges in the graph
    export_json: 0 or 1, optional
        If it is set to 1, a JSON representation will be returned
    
    Returns
    ----------
    [G,data]: [networkx.Graph,dict]
        The graph representation G and the corresponding JSON dict object
    """
    
    data = dict()
    G=nx.DiGraph()
    G.add_nodes_from(node_preds)
    G.add_nodes_from(rule_preds,color='orange')
    G.add_edges_from(edges)
    
    if export_json:
        from networkx.readwrite import json_graph
        data = json_graph.node_link_data(G)
    
    return [G,data]
    
def export_expl_graph(name,G,id=0,path="./",export_json=0):
    """The graph G is exported as a PDF to the specified path.
    Parameters
    ----------
    name: str
        The name of the graph to be generated
    G: networkx.Graph
        The graph to be exported
    path: str, optional
        The location of the graph to be saved
    export_json: 0 or 1, optional
        If it is set to 1, a JSON representation will be returned      
    """
    
    # get AGraph from G graph
    A=nx.nx_agraph.to_agraph(G)
    A.node_attr['shape']='box'
    #A.graph_attr.update(rankdir="LR")
    A.graph_attr.update(ranksep=0.3)
    A.graph_attr.update(nodesep=0.3)
    A.graph_attr.update(ratio="auto")
    A.graph_attr.update(fontname="Helvetica")
    
    A.layout(prog='dot')
    # Write the PDF file to the directory where the example is
    A.draw(path+name+'_'+str(id)+'.pdf')
    
def graph_sub(rootnode,G):
    """Extracts a subgraph from a graph G given a rootnode
    Parameters
    ----------
    rootnode: str
        The rootnode of the subtree
    G: networkx.Graph
        The graph from which the subgraph will be extracted
    Returns
    ----------
    Gsub: networkx.Graph
        The subgraph extracted from G
    """
    
    Gsub=dfs_tree(G.reverse(), rootnode)
    props = dict()
    traces = set() # agent,leaf node,rule count,shortest path to the root
    facts = dict()
    
    leaves= [x for x in Gsub.nodes() if Gsub.out_degree(x)==0]
    
    for leaf in leaves:
        agent=""
        
        if leaf.startswith("trust") or leaf.startswith("best_values") or leaf.startswith("minimise"):
            continue
        
        if leaf.startswith("says"):
            agent = leaf.split("(")[1].split(",")[0]
            
        if leaf.startswith("keep"):
            agent = "me"
            
        # distance to root
        path = nx.shortest_path(Gsub, source=rootnode, target=leaf)
        rcount = 0
        for n in path:
            if n.startswith("rule("):
                rcount = rcount + 1
        traces.add((agent,leaf,rcount,len(path)-1))
            
        # facts
        if agent in facts.keys():
            facts[agent] = facts[agent] + 1
        else:
            facts[agent] = 1
    
    return Gsub

def relevant_agents(model,G):
    """
    Parameters
    ----------
    model: str
        A specific node that specifies a model (i.e. includes share or not_share)
    G: networkx.Graph
        The graph representation of the specified scenario
    Returns
    ----------
    agents_cvs: dict
        A dictionary of agents who provided some information together with their
        corresponding confidence values
    """
    
    agents_cvs = []
    
    Gsub = graph_sub(model,G)
    
    leaves= [x for x in Gsub.nodes() if Gsub.out_degree(x)==0]
    
    for leaf in leaves:
        if leaf.startswith("says"):
            a = leaf.split("(")[1].split(",")[0]
            cv = int(leaf[:-1].split(",")[-1])
            agents_cvs.append((a,cv))
    
    return agents_cvs
