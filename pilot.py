import matplotlib.pyplot as plt
import pandas as pd

   
def update_scenario_file(scenario,path):
    """The generated scenario will be saved to a file in
    the indicated path
    Parameters
    ----------
    scenario: str
        A scenario string to prepare scenario as a dl file
    path: str
        The location to save the scenario file
    """
    
    f= open(path+'/scenario.dl','w+')
    f.write(scenario)
    f.close()

def scenario_generator_all(agents,df_participants):
    """Generates scenarios for all participants
    Parameters
    ----------
    agents: set
        The agent set involved in a scenario
    df_participants: Pandas Dataframe
        The participants dataframe including scenarios and their feedback
    
    Returns
    ----------
    (scenarios,agents_cvs): (dict,dict)
        The set of dl formatted scenarios and the set of agents together
        with confidence values for various features
    """
    data = df_participants.loc[:,"benefit_code":"retention_code"]
    agents_cvs = dict()
    
    scenarios = dict()
    
    upper_CV = 100
    lower_CV = 0
    
    import random
    for a in agents:
        cv1 = random.randint(lower_CV,upper_CV)
        cv2 = random.randint(lower_CV,upper_CV)
        cv3 = random.randint(lower_CV,upper_CV)
        cv4 = random.randint(lower_CV,upper_CV)
        cv5 = random.randint(lower_CV,upper_CV)
        cv6 = random.randint(lower_CV,upper_CV)
        
        agents_cvs[a] = (cv1,cv2,cv3,cv4,cv5,cv6)
    
    scen = ""
    for index, scenario in data.iterrows():
        agent = scenario["device_code"].split("_")[-1]
        
        scen += "says("+agent+","+scenario.iloc[0]+","+str(agents_cvs[agent][0])+").\n"
        scen += "says("+agent+","+scenario.iloc[1]+","+str(agents_cvs[agent][1])+").\n"
        scen += "says("+agent+","+scenario.iloc[2]+","+str(agents_cvs[agent][2])+").\n"
        #scen += "says("+agent+","+scenario.iloc[3]+","+str(agents_cvs[agent][3])+").\n"
        scen += "says("+agent+","+scenario.iloc[4]+","+str(agents_cvs[agent][4])+").\n"
        scen += "says("+agent+","+scenario.iloc[5]+","+str(agents_cvs[agent][5])+").\n"
        
        scenarios[index+1] = scen
        scen = ""
        
        # end of scenarios
        if index == 13:
            break
    
    return scenarios, agents_cvs
    
def get_true_labels(df_scenarios):
    """Returns the participants' feedback for all scenarios
    Parameters
    ----------
    df_scenarios: Pandas Dataframe
        The dataframe of participants including their feedback data
    
    Returns
    ----------
    df_scenXlabels.T: Pandas Dataframe
        The user feedback per scenario for all participants
    """
    
    df_scenXlabels = pd.DataFrame()
    
    for i in range(1,15):
        data = df_scenarios.loc[df_scenarios["id"]==i,["dec"]].loc[:,"dec"]
        #print data.shape
        data.index = range(1,data.shape[0]+1)
    
        df_scenXlabels[i] = data
    
    return df_scenXlabels.T

def plots_per_participant_trust(agents,part_new_trusts,ax=""):
    """A plot per participant
    Parameters
    ----------
    agents: set
        The set of agents involved in a scenario
    part_new_trusts: set
        The set of updated trust values of agents
    ax: ax, optional
        If set, the plot will use the provided ax
    
    Returns
    ----------
    ax: ax
        The updated plot ax
    """
    ### plots
    scenario_labels = range(1,15)
    
    df_trust_changes = pd.DataFrame(part_new_trusts, index=scenario_labels)
    df_trust_changes.columns = agents
    
    if ax == "":
        df_trust_changes.T.plot.bar()
    else:
        df_trust_changes.T.plot.bar(ax=ax)
        
    return ax
    
def plots_trusts(agents,participants_info,tvalue):
    """For each participant, it creates a plot of trust updates
    Parameters
    ----------
    agents: set
        The set of agents involved in a scenario
    participants_info: dict of dict
        A logger object keeping all participants trust values and model
        decisions for different trust values
    
    Returns
    ----------
    fig: plot
        A plot including trust updates for all participants
    """
    plt.subplots_adjust(hspace=0.2, bottom = 0.5)
    number_of_subplots=25
    
    fig, axes = plt.subplots(number_of_subplots/2+1, 2,figsize=(15,35))
    
    k=0
    j=0
    for i in range(0,number_of_subplots):
        plots_per_participant_trust(agents,participants_info[i+1][tvalue][0],ax=axes[k][j])
        axes[k][j].legend(ncol=7)
        axes[k][j].set_ylim(0,100)
        axes[k][j].set_xlabel("IoT Devices")
        axes[k][j].set_ylabel("Trust")
        axes[k][j].set_title('Participant: '+str(i+1))
        
        if j == 1:
            k = k+1        
        
        j = (i-1)%2
    
    fig.tight_layout(pad=2)
    
    return fig
    
def log_plots(agents,participants_info, trust_val):
    """Save the trust updates plot in the logs folder for a specific trust value
    Parameters
    ----------
    agents: set
        The set of agents involved in a scenario
    participants_info: dict of dict
        A logger object keeping all participants trust values and model
        decisions for different trust values
    trust_val: int
        The trust value for which the plot of all participants will be saved
    """
    fig_T = plots_trusts(agents,participants_info,trust_val)
    fig_T.savefig("logs/run-"+str(trust_val)+"-trusts.pdf")
    plt.close(fig_T)
