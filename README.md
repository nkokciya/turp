# TURP

TURP shows how IoT entities can manage the trust they have in others and make 
privacy decisions based on that. This implementation is part of our paper 
"TURP: Managing Trust for Regulating Privacy in Internet of Things" by 
N. Kokciyan, P. Yolum. **[Accepted. To appear in IEEE Internet Computing.]**

# GENERAL STRUCTURE
* dlv_helpers file includes functions to invoke the DLV tool and process the 
results.
* graph_helpers file includes functions to generate and travers graphs. The subgraphs 
are used to find agents involved in specific models.
* pilot file includes auxiliary functions to run the pilot study that is based
on survey data including 14 scenarios labelled by 25 participants.
* parse_iot file include the proposed trust management model. The code to run the 
scenario and the pilot study described in our paper is also provided.

# RUNNING THE PHONE SCENARIO

* trust-rules.dl file includes facts and rules of the phone agent.
* The python code to use for running the example to compute all the models is as 
follows: `python parse_iot.py phone`
* All the execution steps are logged in logs/run.log file.

## Did I run the scenario correctly? 

You should get the following results after running the scenario:
* A PDF file in the phone folder. 
* A run log in the logs folder.

The output/phone folder includes these two outputs already. You can check if you get 
the same results. If not, try again. And let me know if you need
further help to run the code.

# RUNNING THE PILOT STUDY
* survey-rules.dl file includes facts and rules generated based on 31 surveys.
* An example survey data is provided in the data folder.
* The python code to use for running the example to compute all the models is as 
follows: `python parse_iot.py pilot`
* All the execution steps are logged in logs/run.log file.
* For different initial trust values, trust update plots will be generated for 
25 participants. Each run will generate new results in the logs folder.

## Did I run the pilot study correctly? 

You should get the following results after running the pilot:
* A run log in the logs folder.
* Seven trust update plot PDF files in the logs folder.

The output/pilot folder includes these example outputs already. You can check if 
you get similar results. If not, try again. And let me know if you need
further help to run the code. Note that each run will generate new results.

# REQUIREMENTS

*  [DLV](http://www.dlvsystem.com/dlv/) reasoner should be installed. dlv script 
should be located in the code folder. The script is already included in the root
folder for your convenience. Before running the code, make sure that this file
is an executable file on your system. 
*  The code was tested with Python 2.7.16 on a Linux machine (Ubuntu 18.04.4 LTS) 
and on a Mac machine (macOS Mojave 10.14.6). 
*  requirements.txt includes more information about the required packages.


